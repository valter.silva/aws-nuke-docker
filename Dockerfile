FROM ubuntu:16.04

ARG AWS_NUKE_VERSION
ARG AWS_ACCESS_KEY_ID
ARG AWS_SECRET_ACCESS_KEY
ARG AWS_REGION
ARG AWS_OUTPUT

# Use Australian Ubuntu Mirror for better connectivity
RUN sed -i 's,archive.ubuntu.com,mirror.waia.asn.au,g' /etc/apt/sources.list

RUN apt-get update

RUN apt-get -qq install -y curl rename vim wget

WORKDIR /aws-nuke

RUN wget --quiet https://github.com/rebuy-de/aws-nuke/releases/download/${AWS_NUKE_VERSION}/aws-nuke-${AWS_NUKE_VERSION}-linux-amd64.tar.gz

RUN tar -xzvf aws-nuke-${AWS_NUKE_VERSION}-linux-amd64.tar.gz -C /usr/local/bin

RUN mv /usr/local/bin/aws-nuke-${AWS_NUKE_VERSION}-linux-amd64 /usr/local/bin/aws-nuke

RUN curl --silent https://bootstrap.pypa.io/get-pip.py -o get-pip.py

RUN apt-get -qq install -y python

RUN python --version

RUN python get-pip.py

RUN pip install awscli 

RUN aws --version

RUN mkdir ~/.aws/

COPY aws-credentials ~/.aws/credentials
COPY aws-config ~/.aws/config

COPY nuke-config.yml .

# clean
RUN rm -f *.tar.gz *.py

ENTRYPOINT [ "/bin/bash" ]