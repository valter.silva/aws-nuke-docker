.ONESHELL:

SHELL = /bin/sh

HEADER = "*****"
BUILD_IMAGE_NAME:=aws-nuke
CONTAINER_NAME=$(BUILD_IMAGE_NAME)-run
BUILDER_IMAGE_NAME:=ubuntu:16.04
CURRENT_FOLDER_PATH=$(shell pwd)

.PHONY: build
build: ## Builds the project within Docker
	@echo "Building Docker image $(BUILD_IMAGE_NAME)"
	docker build --force-rm -t $(BUILD_IMAGE_NAME) --build-arg AWS_NUKE_VERSION=v2.7.0 .
	# docker run --rm -v $(CURRENT_FOLDER_PATH):/home/gradle/src -w /home/gradle/src gradle:jdk8 gradle build

.PHONY: run
run: ## Run the project in a Docker container
	docker run --name $(CONTAINER_NAME) --rm --detach $(BUILD_IMAGE_NAME) bash

.PHONY: exec
exec: run ## Runs and executes a command inside of the Docker container
	docker exec -it $(CONTAINER_NAME) ls -ltha

.PHONY: stop
stop: ## Stop the $(BUILD_IMAGE_NAME)-run running container
	-docker stop $(BUILD_IMAGE_NAME)-run

.PHONY: prune
prune: ## Remove dangling images
	@echo "Remove dangling images"
	-docker image prune --force

.PHONY: destroy
destroy: stop prune ## Destroy docker containers
	@echo "Remove Docker image $(BUILD_IMAGE_NAME)"
	-docker rmi --force $(BUILD_IMAGE_NAME)

.PHONY: destroy-all-containers
destroy-all-containers: ## Stops and destroy all Docker containers
	@echo "Stop all Docker running containers"
	-docker stop $(shell docker ps -aq)
	@echo "Remove all stopped Docker containers"
	-docker rm $(shell docker ps -aq)

.PHONY: destroy-all-images
destroy-all-images:
	@echo "Remove all Docker images"
	-docker rmi $(shell docker images -aq)

.PHONY: destroy-all
destroy-all: destroy-all-containers destroy-all-images ## Destroy all docker containers and images
	make list

.PHONY: bootstrap
bootstrap: destroy build destroy ## Destroys previous build, builds the project, and destroys the project

.PHONY: list
list: ## List all docker images and containers
	@echo "***** RUNNING DOCKER CONTAINERS *****"
	docker ps -a
	@echo "***** RUNNING DOCKER IMAGES *****"
	docker images -a

help: ## Show all commands available and their description
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.DEFAULT_GOAL := help
