# AWS Nuke
Run aws-nuke on Docker container


## How to get started

Paste your `AWS` credentials and configuration into `aws-credentials` and `aws-config` respectively.

Build the Docker image

```
make build
```

### How to run the Docker container

```
make run
```

## Help

```
make help
```

## How to run

```
aws-nuke --config nuke-config.yml --access-key-id <access-key-id> --secret-access-key <secret-access-key> --no-dry-run
```